import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse
import os

def req_try(url):
    try:
        page = requests.get(url)
    except requests.RequestException as e:
        print(e)
        exit()
    except requests.ConnectionError as e:
        print(e)
        exit()
    except requests.HTTPError as e:
        print(e)
        exit()
    except requests.TooManyRedirects as e:
        print(e)
        exit()
    except requests.ConnectTimeout as e:
        print(e)
        exit()
    except requests.ReadTimeout as e:
        print(e)
        exit()
    except requests.Timeout as e:
        print(e)
        exit()
    return(page)


def scrapper(region):
    # Get the list of routes in region
    url = f"https://www.16seats.net/chi/gmb/g{region[0]}_list.html"
    page = 0
    page = req_try(url)
    soup = BeautifulSoup(page.content, "html.parser")
    url_list = soup.find(id="SvcList").find_all("a")

    # Create region folder
    try:
        os.mkdir(f"green-{region}/")
    except OSError:
        pass

    # Get the Google iframe link from each page then download the kml files
    url = "https://www.16seats.net/chi/gmb/"
    for i in url_list:
        href = f"{url}{i.get('href')}"
        # Some minibus routes share the same page. Repeated pages are skipped.
        if ("#" in href):
            continue
        page = req_try(href)
        soup = BeautifulSoup(page.content, "html.parser")
        iframe_list = soup.find_all("iframe")
        
        # Counter for routes: from = 0, to = 1, special routes >= 2
        counter = 0
        for j in iframe_list:
            kml = req_try(
                f"https://www.google.com/maps/d/kml?{urlparse(j.get('src')).query}")
            open(f"green-{region}/green-{region}-{i.text.split()[0]}-{counter}.kml",
                 "wb").write(kml.content)
            print(
                f"Downloaded green-{region}-{i.text.split()[0]}-{counter}.kml")
            counter += 1


def main():
    scrapper('hong-kong-island')
    scrapper('kowloon')
    scrapper('new-territories')


if __name__ == '__main__':
    main()
